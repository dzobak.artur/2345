﻿using System;
using System.Collections.Generic;
using System.IO;


interface IMessageSender
{
    void SendMessage(TextMessage message);
}


class TextMessage
{
    public string Text { get; set; }
    public DateTime TimeSent { get; set; }
    public string ReceiverNumber { get; set; }

    public TextMessage(string text, DateTime timeSent, string receiverNumber)
    {
        Text = text;
        TimeSent = timeSent;
        ReceiverNumber = receiverNumber;
    }
}


class ConsoleMessageSender : IMessageSender
{
    public void SendMessage(TextMessage message)
    {
        Console.WriteLine($"Відправлено: {message.Text} ({message.TimeSent}) до {message.ReceiverNumber}");
    }
}


abstract class MessageDecorator : IMessageSender
{
    protected IMessageSender _messageSender;

    public MessageDecorator(IMessageSender messageSender)
    {
        _messageSender = messageSender;
    }

    public virtual void SendMessage(TextMessage message)
    {
        _messageSender.SendMessage(message);
    }
}


class LoggingDecorator : MessageDecorator
{
    private readonly string _logFileName;

    public LoggingDecorator(IMessageSender messageSender, string logFileName)
        : base(messageSender)
    {
        _logFileName = logFileName;
    }

    public override void SendMessage(TextMessage message)
    {
        using (StreamWriter writer = File.AppendText(_logFileName))
        {
            writer.WriteLine($"Відправлено: {message.Text} ({message.TimeSent}) до {message.ReceiverNumber}");
        }

        base.SendMessage(message);
    }
}


class BlacklistCheckDecorator : MessageDecorator
{
    private readonly List<string> _blacklist;

    public BlacklistCheckDecorator(IMessageSender messageSender, List<string> blacklist)
        : base(messageSender)
    {
        _blacklist = blacklist;
    }

    public override void SendMessage(TextMessage message)
    {
        if (_blacklist.Contains(message.ReceiverNumber))
        {
            throw new Exception($"Номер отримувача {message.ReceiverNumber} знаходиться в чорному списку.");
        }

        base.SendMessage(message);
    }
}

internal class Program
{
    static void Main(string[] args)
    {
        IMessageSender messageSender = new ConsoleMessageSender();

        List<string> blacklist = new List<string> { "123456", "9873654" };

        
        IMessageSender decoratedMessageSender = new LoggingDecorator(
            new BlacklistCheckDecorator(messageSender, blacklist), "message_log.txt");

        TextMessage message1 = new TextMessage("Привіт!", DateTime.Now, "555555");
        TextMessage message2 = new TextMessage("Важливе повідомлення", DateTime.Now, "123456");

        try
        {
            decoratedMessageSender.SendMessage(message1);
            decoratedMessageSender.SendMessage(message2);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Помилка: {ex.Message}");
        }
    }
}
